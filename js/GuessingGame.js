"use strict";

let GuessingGame = {};
GuessingGame.guessesMade = 0;

$(document).ready(function () {

    GuessingGame.f_newGame = function () {
        $("#guessCountDown").children().hide();
        $("#GuessCounter3").show();
        $(".guessNumber").removeClass("correct").removeClass("wrong");
        $("#guessNumberHolder").show();
        GuessingGame.guessesMade = 0;

        GuessingGame.anwser = Library.getRandomInt(1, 10);
    };

    // Checks if a guess made was correct or not.
    GuessingGame.f_checkGuess = function (val) {

            val = parseInt(val);

            if(isNaN(val)){
                GuessingGame.setGuessesMade(GuessingGame.guessesMade+1);
                $(`#guessingGameGuess${GuessingGame.guessesMade}`).html("You idiot, that's not a number.");

                throw "f_checkGuess Invalid input";
            }



        let guess = $(`.guessNumber[guess="${val}"]`);

        if (guess.attr("class").includes("wrong")){
            return;
        }

        if (GuessingGame.anwser < val){
            console.log(GuessingGame.anwser + " is smaller than Val: " + val);
            GuessingGame.setGuessesMade(GuessingGame.guessesMade+1);
            $(`#guessingGameGuess${GuessingGame.guessesMade}`).html(`${val} is larger`);
            guess.addClass("wrong");
        }
        else if(GuessingGame.anwser === val){
            $("#guessNumberHolder").hide();
            $("#guessingGameResult").hide();
            $("#guessCountDown").children().hide();
            $("#GuessingGameWon").show();
        }
        else if(val < GuessingGame.anwser){
            console.log("Val: " + val + " is smaller than " + GuessingGame.anwser);
            GuessingGame.setGuessesMade(GuessingGame.guessesMade+1);
            $(`#guessingGameGuess${GuessingGame.guessesMade}`).html(`${val} is smaller`);
            guess.addClass("wrong");
        }

    };

    // This function updates the amount of guesses made
    GuessingGame.setGuessesMade = function (val) {

        // Game has been lost
        if(val > 3){
            val = 3;
        }

        let guessCountDown = $("#guessCountDown");

        if(val === 3){
            $("#guessNumberHolder").hide();
            $("#guessingGameResult").hide();
            guessCountDown.children().hide();
            $("#GuessingGameLost").show();
            GuessingGame.guessesMade = val;
            return;
        }

        if(val < 0){
            throw new Error("Value out of bounds");
        }

        guessCountDown.children().hide();
        $(`#GuessCounter${3-val}`).show();

        GuessingGame.guessesMade = val;
    };

    GuessingGame.f_newGame();

    $("#newNumber").on("click", function () {
       GuessingGame.f_newGame();
    });

    $(".guessNumber").on("click", function (e) {
        let number = $(e.target).attr("guess");
        GuessingGame.f_checkGuess(number);
    });

});