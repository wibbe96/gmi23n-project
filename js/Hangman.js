"use strict";

let Hangman = {};
Hangman.guessesMade = 0;

$(document).ready(function () {

    Hangman.f_NewWord = function (word) {

        word = word.toUpperCase();

        // Resets the guessing buttons so they are normal
       let guess = $(".guessLetter");
        guess.removeClass("correct");
        guess.removeClass("wrong");
        $("#guessLetterHolder").show();
        $("#gameWon").hide();
        $("#gameLost").hide();
        Hangman.setGuessesMade(0);

        //Rests the word div
        $("#word").html("");

        //Splits the new word up and puts it into the Hangman.word variable
        Hangman.word = word.split("");

        //Goes over each letter in the word and adds it to the page
        Hangman.word.forEach( function (value){
            Hangman.f_addEmptyGuess(value);
        });

    };

    //This function adds a letter to the word div so that it can be guessed on.
    Hangman.f_addEmptyGuess = function (val) {

        let html = "";

        val = val.toUpperCase();

        const regex = new RegExp("[A-Z]");

        if (!regex.test(val)){
            html = `<div><p class="wordLetter"> ${val} </p></div>`;
        }
        else {
            html = `<div><p class="wordLetter unknownLetter" letter="${val}"> _ </p></div>`;
        }

        $("#word").append(html);

    };

    // Checks if a guess made was correct or not.
    Hangman.f_checkGuess = function (val) {

       let found = $(`.wordLetter[letter="${val}"]`);
        let guess = $(`.guessLetter[guess="${val}"]`);
        if (guess.attr("class").includes("wrong") || guess.attr("class").includes("correct")){
            return;
        }

        if (found.length === 0){
            guess.addClass("wrong");
            Hangman.setGuessesMade(Hangman.guessesMade+1);
        }
        else {
            $.each(found, function (index, value) {
                $(value).html(val);
                $(value).removeClass("unknownLetter");
            });
            guess.addClass("correct");
        }

        if($(".unknownLetter").length === 0){
            $("#guessLetterHolder").hide();
            $("#gameWon").show();
        }

    };

    // This function updates the amount of guesses made
    Hangman.setGuessesMade = function (val) {

        // Game has been lost
        if(val > 7){
            val = 7;
        }

        if(val === 7){
            $("#guessLetterHolder").hide();
            $("#gameLost").show();
        }

        if(val < 0){
            throw new Error("Value out of bounds");
        }

        $(".imageGuess").hide();
        $(`.imageGuess[guess="${val-1}"]`).show();

        Hangman.guessesMade = val;
    };

    $("#newWord").on("click", function () {
       Hangman.f_NewWord($("#input").val());
    });

    $(".guessLetter").on("click", function (e) {
        const target = e.target;
        const letter = $(target).attr("guess");

        Hangman.f_checkGuess(letter);
    })

});