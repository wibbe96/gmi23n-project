
$(document).ready(function () {

    QUnit.module("HelperLibrary", function () {

        const {test} = QUnit;

        test("getRandomInt_CheckGeneratedNumbers", t => {
            for (let i = 0; i < 10; i++){
                let rand = Library.getRandomInt(0, 10);
                let result = 0 <= rand <= 10;
                t.ok(result, rand + " is between 0 and 10");
            }
        });

    });

});
