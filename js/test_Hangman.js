
$(document).ready(function () {

    QUnit.module("Hangman", function () {

        const {test} = QUnit;

        test("setGuessesMade_GameLost", t => {

            let gameLost = $("#gameLost");
            let guessLetterHolder = $("#guessLetterHolder");
            let guessLetter = $(".guessLetter");

            Hangman.setGuessesMade(1);
            let display = gameLost.css("display");
            let display2 = guessLetterHolder.css("display");
            let disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 1");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 1");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(2);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 2");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 2");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(3);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 3");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 3");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(4);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 4");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 4");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(5);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 5");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 5");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(6);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 6");
            t.equal(display2, "flex", "guessLetterHolder is shown when guesses made are 6");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(7);
            display = gameLost.css("display");
            display2 = guessLetterHolder.css("display");
            t.equal(display, "block", "GameLost is shown when guesses made are 7");
            t.equal(display2, "none", "guessLetterHolder is hidden when guesses made are 7");
            mock_Reset_Hangman();

        });

        test("setGuessesMade_CorrectValueSet", t => {
            Hangman.setGuessesMade(0);
            t.equal( Hangman.guessesMade,0, "Hangman.setGuessesMade has set Hangman.guessesMade to 0");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(1);
            t.equal( Hangman.guessesMade,1, "Hangman.setGuessesMade has set Hangman.guessesMade to 1");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(2);
            t.equal( Hangman.guessesMade,2, "Hangman.setGuessesMade has set Hangman.guessesMade to 2");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(3);
            t.equal( Hangman.guessesMade,3, "Hangman.setGuessesMade has set Hangman.guessesMade to 3");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(4);
            t.equal( Hangman.guessesMade,4, "Hangman.setGuessesMade has set Hangman.guessesMade to 4");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(5);
            t.equal( Hangman.guessesMade,5, "Hangman.setGuessesMade has set Hangman.guessesMade to 5");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(6);
            t.equal( Hangman.guessesMade,6, "Hangman.setGuessesMade has set Hangman.guessesMade to 6");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(7);
            t.equal( Hangman.guessesMade,7, "Hangman.setGuessesMade has set Hangman.guessesMade to 7");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(8);
            t.equal( Hangman.guessesMade,7, "Hangman.setGuessesMade has set Hangman.guessesMade to 8");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(9);
            t.equal( Hangman.guessesMade,7, "Hangman.setGuessesMade has set Hangman.guessesMade to 9");
            mock_Reset_Hangman();

        });

        test("setGuessesMade_IncorrectValueSet", t => {

            try {
                Hangman.setGuessesMade(-1);
            }
            catch(e)
            {
                t.equal(e.message, "Value out of bounds", "Hangman.setGuessesMade(-1) has thrown an error successfully");
            }
            mock_Reset_Hangman();

        });

        test("setGuessesMade_ImageGuessShown", t => {

            Hangman.setGuessesMade(1);
            let display = $(`.imageGuess[guess="0"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 1 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(2);
            display = $(`.imageGuess[guess="1"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 2 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(3);
            display = $(`.imageGuess[guess="2"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 3 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(4);
            display = $(`.imageGuess[guess="3"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 4 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(5);
            display = $(`.imageGuess[guess="4"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 5 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(6);
            display = $(`.imageGuess[guess="5"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 6 is shown");
            mock_Reset_Hangman();

            Hangman.setGuessesMade(7);
            display = $(`.imageGuess[guess="6"]`).css("display");
            t.equal(display, "inline", "imageGuess for guess 7 is shown");
            mock_Reset_Hangman();

        });

        test("f_NewWord_Reset", t => {

            let guessLetterHolder = $("#guessLetterHolder");
            let gameWon = $("#gameWon");
            let gameLost = $("#gameLost");

            // Fucks up the page to see if f_NewWord can reset it properly
            const guess = $(".guessLetter");
            guess.addClass("correct");
            guess.addClass("wrong");
            guess.addClass("disabled");
            guessLetterHolder.hide();
            gameWon.show();
            gameLost.show();
            Hangman.setGuessesMade(100);


            Hangman.f_NewWord("TEST");

            // Asserts if the correct, wrong and disabled classes have been removed.
            let classes = guess.attr("class").split(" ");
            t.ok(!classes.includes("correct"), "guessLetter does not have class correct");
            t.ok(!classes.includes("correct"), "guessLetter does not have class wrong");
            t.ok(!classes.includes("correct"), "guessLetter does not have class disabled");

            // Checks that guessLetterHolder has been restored
            let display = guessLetterHolder.css("display");
            t.equal(display, "flex", "guessLetter does not have class disabled");

            // Checks that gameWon has been restored
            display = gameWon.css("display");
            t.equal(display, "none", "gameWon does not show");

            // Checks that gameWon has been restored
            display = gameLost.css("display");
            t.equal(display, "none", "gameLost does not show");

            // Checks the number of guesses made is reset to 0
            t.equal(Hangman.guessesMade, 0, "guessesMade is reset to 0");
            mock_Reset_Hangman();

        });

        test("f_NewWord_WordCreation", t => {

            let word = $("#word");

            word.html("<p>Test</p>");
            Hangman.f_NewWord("TEST");
            t.notEqual(word.html(), "", "word has been created");

            t.deepEqual(Hangman.word, ["T", "E", "S", "T"], "Hangman.word is correct");

            let numberOfLetters = $(".wordLetter");
            t.equal(numberOfLetters.length, 4, "Correct number of letters added");

            mock_Reset_Hangman();
        });

        test("f_NewWord_ClickTest_WordCreation", t => {

            $("#input").val("Test");
            $("#newWord").trigger("click");

            t.deepEqual(Hangman.word, ["T", "E", "S", "T"], "Hangman.word is correct");

            let numberOfLetters = $(".wordLetter");
            t.equal(numberOfLetters.length, 4, "Correct number of letters added");

            mock_Reset_Hangman();
        });

        test("f_NewWord_WordCreation_NonLetterSymbol", t => {

            Hangman.f_NewWord("odd-looking");
            let html = $(".wordLetter:not([letter])").html();
            t.equal(html, " - ", "Non letter symbol added successfully.");
            mock_Reset_Hangman();
        });

        test("f_checkGuess_TestGuess", t => {

            Hangman.f_NewWord("TEST");
            Hangman.f_checkGuess("T");

            let html = $(`.wordLetter[letter="T"]`).html();
            t.equal(html, "T", "Guessed successfully.");
            mock_Reset_Hangman();
        });

        test("f_checkGuess_ClickTest_TestGuess", t => {

            Hangman.f_NewWord("TEST");

            $(".guessLetter").trigger("click");

            let html = $(`.wordLetter[letter="T"]`).html();
            t.equal(html, "T", "Guessed successfully.");
            mock_Reset_Hangman();
        });

        test("f_checkGuess_TestWin", t => {

            Hangman.f_NewWord("TEST");
            Hangman.f_checkGuess("T");
            Hangman.f_checkGuess("E");
            Hangman.f_checkGuess("S");


            // Checks that gameWon has been restored
            let display = $("#gameWon").css("display");
            t.equal(display, "block", "gameWon shown, game won");
            mock_Reset_Hangman();

        });

        test("f_checkGuess_TestLose", t => {

            Hangman.f_NewWord("TEST");
            Hangman.f_checkGuess("A");
            Hangman.f_checkGuess("B");
            Hangman.f_checkGuess("C");
            Hangman.f_checkGuess("D");
            Hangman.f_checkGuess("F");
            Hangman.f_checkGuess("G");
            Hangman.f_checkGuess("H");

            // Checks that gameWon has been restored
            let display = $("#gameLost").css("display");
            t.equal(display, "block", "gameLost shown, game lost");
            mock_Reset_Hangman();

        });

        test("f_checkGuess_TestDuplicateGuess", t => {

            let HTML = $("html");

            Hangman.f_NewWord("TEST");
            Hangman.f_checkGuess("E");

            let html = HTML.html();
            Hangman.f_checkGuess("E");

            // Checks that gameWon has been restored
            t.deepEqual(html, HTML.html(), "Two correct guesses made");
            mock_Reset_Hangman();

            Hangman.f_NewWord("TEST");
            Hangman.f_checkGuess("A");

            html = HTML.html();
            Hangman.f_checkGuess("A");

            // Checks that gameWon has been restored
            t.deepEqual(html, HTML.html(), "Two incorrect guesses made");
            mock_Reset_Hangman();

        });

    });
    
    function mock_Reset_Hangman() {
        $(".imageGuess").hide();
        $(".guessLetter").attr("state", "active");
        $("#gameWon").hide();
        $("#gameLost").hide();
        $("#word").html("");
        Hangman.word = null;
        Hangman.guessesMade = 0;
    }
});
