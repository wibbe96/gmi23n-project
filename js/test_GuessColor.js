$(document).ready(function () {

    QUnit.module("GuessColor", function () {

        const {test} = QUnit;

        test("GuessColor_playName2RGB", t => {
            GuessColor.playName2RGB();
            t.ok(checkNameOutput(), "GuessColour.playName2RGB creates the correct output");
            t.ok(checkRGBInput(), "GuessColour.playName2RGB creates the correct input");
        });

        test("GuessColor_playName2Hex", t => {
            GuessColor.playName2Hex();
            t.ok(checkNameOutput(), "GuessColour.playName2Hex creates the correct output");
            t.ok(checkHexInput(), "GuessColour.playName2Hex creates the correct input");
        });

        test("GuessColor_playName2Colour", t => {
            GuessColor.playName2Colour();
            t.ok(checkNameOutput(), "GuessColour.playName2Colour creates the correct output");
            t.ok(checkColorInput(), "GuessColour.playName2Colour creates the correct input");
        });


        test("GuessColor_playRGB2Name", t => {
            GuessColor.playRGB2Name();
            t.ok(checkRGBOutput(), "GuessColour.playRGB2Name creates the correct output");
            t.ok(checkNameInput(), "GuessColour.playRGB2Name creates the correct input");
        });

        test("GuessColor_playRGB2Hex", t => {
            GuessColor.playRGB2Hex();
            t.ok(checkRGBOutput(), "GuessColour.playRGB2Hex creates the correct output");
            t.ok(checkHexInput(), "GuessColour.playRGB2Hex creates the correct input");
        });

        test("GuessColor_playRGB2Colour", t => {
            GuessColor.playRGB2Colour();
            t.ok(checkRGBOutput(), "GuessColour.playRGB2Colour creates the correct output");
            t.ok(checkColorInput(), "GuessColour.playRGB2Colour creates the correct input");
        });


        test("GuessColor_playHex2Name", t => {
            GuessColor.playHex2Name();
            t.ok(checkHexOutput(), "GuessColour.playHex2Name creates the correct output");
            t.ok(checkNameInput(), "GuessColour.playHex2Name creates the correct input");
        });

        test("GuessColor_playHex2RGB", t => {
            GuessColor.playHex2RGB();
            t.ok(checkHexOutput(), "GuessColour.playHex2RGB creates the correct output");
            t.ok(checkRGBInput(), "GuessColour.playHex2RGB creates the correct input");
        });

        test("GuessColor_playHex2Color", t => {
            GuessColor.playHex2Colour();
            t.ok(checkHexOutput(), "GuessColour.playHex2Colour creates the correct output");
            t.ok(checkColorInput(), "GuessColour.playHex2Colour creates the correct input");
        });


        test("GuessColor_playColor2Name", t => {
            GuessColor.playColour2Name();
            t.ok(checkColorOutput(), "GuessColour.playColour2Name creates the correct output");
            t.ok(checkNameInput(), "GuessColour.playColour2Name creates the correct input");
        });

        test("GuessColor_playColor2RGB", t => {
            GuessColor.playColour2RGB();
            t.ok(checkColorOutput(), "GuessColour.playColour2RGB creates the correct output");
            t.ok(checkRGBInput(), "GuessColour.playColour2RGB creates the correct input");
        });

        test("GuessColor_playColor2Hex", t => {
            GuessColor.playColour2Hex();
            t.ok(checkColorOutput(), "GuessColour.playColour2Hex creates the correct output");
            t.ok(checkHexInput(), "GuessColour.playColour2Hex creates the correct input");
        });


        test("GuessColor_reset", t => {

            GuessColor.NameHolder.html("Test");
            GuessColor.RGBHolder.html("Test");
            GuessColor.HexHolder.html("Test");
            GuessColor.ColourHolder.html("Test");
            GuessColor.C1 = null;
            GuessColor.C2 = null;
            GuessColor.C3 = null;
            GuessColor.Correct = null;

            GuessColor.reset();

            t.notEqual(GuessColor.NameHolder.html(), "Test", "GuessColour.reset restores GuessColor.NameHolder.html()");
            t.notEqual(GuessColor.RGBHolder.html(), "Test", "GuessColour.reset restores GuessColor.RGBHolder.html()");
            t.notEqual(GuessColor.HexHolder.html(), "Test", "GuessColour.reset restores GuessColor.HexHolder.html()");
            t.notEqual(GuessColor.ColourHolder.html(), "Test", "GuessColour.reset restores GuessColor.ColourHolder.html()");

            t.notEqual(GuessColor.C1, null, "GuessColour.reset restores GuessColor.C1");
            t.notEqual(GuessColor.C2, null, "GuessColour.reset restores GuessColor.C2");
            t.notEqual(GuessColor.C3, null, "GuessColour.reset restores GuessColor.C3");
            t.notEqual(GuessColor.Correct, null, "GuessColour.reset restores GuessColor.Correct");
        });

        test("GuessColor_attachListeners_CorrectClick", t => {

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C1;

            $("#guessC1").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have won.", "GuessColour.attachListeners works for C1 correct guess");

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C2;

            $("#guessC2").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have won.", "GuessColour.attachListeners works for C2 correct guess");

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C3;

            $("#guessC3").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have won.", "GuessColour.attachListeners works for C3 correct guess");
        });

        test("GuessColor_attachListeners_WrongClick", t => {

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C2;

            $("#guessC1").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have lost, loser.", "GuessColour.attachListeners works for C1 wrong guess");

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C3;

            $("#guessC2").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have lost, loser.", "GuessColour.attachListeners works for C2 wrong guess");

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C1;

            $("#guessC3").trigger("click");

            t.equal(GuessColor.StatusHolder.html(), "You have lost, loser.", "GuessColour.attachListeners works for C3 wrong guess");
        });

        test("GuessColor_guessCorrect", t => {

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C1;

            GuessColor.guess(1);

            t.equal(GuessColor.StatusHolder.html(), "You have won.", "GuessColour.guess functions for a correct guess");
        });

        test("GuessColor_guessWrong", t => {

            GuessColor.playHex2Colour();
            GuessColor.Correct = GuessColor.C1;

            GuessColor.guess(2 );

            t.equal(GuessColor.StatusHolder.html(), "You have lost, loser.", "GuessColour.guess functions for a wrong guess");
        });

        test("GuessColor_guess_GameModeTrigger", t => {

            GuessColor.playHex2Colour();

            GuessColor.gameMode = "playColour2Hex";
            GuessColor.guess(2 );

            t.ok(checkColorOutput() && checkHexInput(), "GuessColour.guess triggers the next game and switches to the correct game mode");
        });

        test("GuessColor_attachMenuListener", t => {

            let menus = $(".gamePicker").find("P");
            let option = [
                "playName2RGB",
                "playName2Hex",
                "playName2Colour",
                "playRGB2Name",
                "playRGB2Hex",
                "playRGB2Colour",
                "playHex2Name",
                "playHex2RGB",
                "playHex2Colour",
                "playColour2Name",
                "playColour2RGB",
                "playColour2Hex"
            ];

            for(let i = 0; i < 12; i++){
                    $(menus[i]).trigger("click");
                    console.log(GuessColor.gameMode);
                    t.equal(GuessColor.gameMode, option[i], "attachMenuListener triggers the correct gameMode");
            }

        });

    });
    
    function checkNameInput() {
        let children = GuessColor.NameHolder.children();
        if ( `<p id="guessC1">${GuessColor.C1.getName()}</p>` !== children[0].outerHTML){
            return false;
        }
        if ( `<p id="guessC2">${GuessColor.C2.getName()}</p>` !== children[1].outerHTML){
            return false;
        }
        if ( `<p id="guessC3">${GuessColor.C3.getName()}</p>` !== children[2].outerHTML){
            return false;
        }
        return GuessColor.NameHolder.children().length === 3;
    }

    function checkNameOutput() {
        if (`<p>${GuessColor.Correct.getName()}</p>` !== GuessColor.NameHolder.html()){
            return false;
        }
        return GuessColor.NameHolder.children().length === 1;
    }

    function checkRGBInput() {
        let children = GuessColor.RGBHolder.children();
        if ( `<p id="guessC1">${GuessColor.C1.getRGB().toString()}</p>` !== children[0].outerHTML){
            console.log("checkRGBInput C1 failed");
            console.log(children[0].outerHTML);
            console.log(`<p id="guessC1">${GuessColor.C1.getRGB().toString()}</p>`);
            return false;
        }
        if ( `<p id="guessC2">${GuessColor.C2.getRGB().toString()}</p>` !== children[1].outerHTML){
            console.log("checkRGBInput C2 failed");
            console.log(children[1].outerHTML);
            console.log(`<p id="guessC2">${GuessColor.C2.getRGB().toString()}</p>`);
            return false;
        }
        if ( `<p id="guessC3">${GuessColor.C3.getRGB().toString()}</p>` !== children[2].outerHTML){
            console.log("checkRGBInput C3 failed");
            console.log(children[2].outerHTML);
            console.log(`<p id="guessC2">${GuessColor.C3.getRGB().toString()}</p>`);
            return false;
        }
        return children.length === 3;
    }

    function checkRGBOutput() {
        if ( `<p>${GuessColor.Correct.getRGB().toString()}</p>` !== GuessColor.RGBHolder.html()){
            return false;
        }
        return GuessColor.RGBHolder.children().length === 1;
    }

    function checkHexInput() {
        let children = GuessColor.HexHolder.children();
        if ( `<p id="guessC1">${GuessColor.C1.getHex()}</p>` !== children[0].outerHTML){
            return false;
        }
        if ( `<p id="guessC2">${GuessColor.C2.getHex()}</p>` !== children[1].outerHTML){
            return false;
        }
        if ( `<p id="guessC3">${GuessColor.C3.getHex()}</p>` !== children[2].outerHTML){
            return false;
        }
        return GuessColor.HexHolder.children().length === 3;
    }

    function checkHexOutput() {
        if (`<p>${GuessColor.Correct.getHex()}</p>` !== GuessColor.HexHolder.html()){
            return false;
        }
        return GuessColor.HexHolder.children().length === 1;
    }

    function checkColorInput() {
        let children = GuessColor.ColourHolder.children();
        if ( `<div class="guessColor" id="guessC1" style="background: rgb(${GuessColor.C1.getR()},${GuessColor.C1.getG()},${GuessColor.C1.getB()})"></div>`
            !== children[0].outerHTML){
            console.log("checkColorInput C1 failed");
            console.log(children[0].outerHTML);
            console.log(`<div class="guessColor" id="guessC1" style="background: rgb(${GuessColor.C1.getR()},${GuessColor.C1.getG()},${GuessColor.C1.getB()})"></div>`);
            return false;
        }
        if ( `<div class="guessColor" id="guessC2" style="background: rgb(${GuessColor.C2.getR()},${GuessColor.C2.getG()},${GuessColor.C2.getB()})"></div>`
            !== children[1].outerHTML){
            console.log("checkColorInput C2 failed");
            console.log(children[1].outerHTML);
            console.log(`<div class="guessColor" id="guessC2" style="background: rgb(${GuessColor.C2.getR()},${GuessColor.C2.getG()},${GuessColor.C2.getB()})"></div>`);
            return false;
        }
        if ( `<div class="guessColor" id="guessC3" style="background: rgb(${GuessColor.C3.getR()},${GuessColor.C3.getG()},${GuessColor.C3.getB()})"></div>`
            !== children[2].outerHTML){
            console.log("checkColorInput C3 failed");
            console.log(children[2].outerHTML);
            console.log(`<div class="guessColor" id="guessC3" style="background: rgb(${GuessColor.C3.getR()},${GuessColor.C3.getG()},${GuessColor.C3.getB()})"></div>`);
            return false;
        }
        if (GuessColor.ColourHolder.children().length !== 3)
        {
            console.log("checkColorInput length failed");
            return false
        }

        return true;

    }

    function checkColorOutput() {
        if (`<div class="guessColor" style="background: rgb(${GuessColor.Correct.getR()},${GuessColor.Correct.getG()},${GuessColor.Correct.getB()})"></div>` !== GuessColor.ColourHolder.html()){
            return false;
        }
        return GuessColor.ColourHolder.children().length === 1;
    }


});