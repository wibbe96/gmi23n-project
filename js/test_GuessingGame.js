
$(document).ready(function () {

    QUnit.module("GuessingGame", function () {

        const {test} = QUnit;

        test("setGuessesMade_GameLost", t => {

            let GuessingGameLost = $("#GuessingGameLost");
            let guessNumberHolder = $("#guessNumberHolder");
            let guessLetter = $(".guessLetter");

            GuessingGame.setGuessesMade(1);
            let display = GuessingGameLost.css("display");
            let display2 = guessNumberHolder.css("display");
            let disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 1");
            t.equal(display2, "flex", "guessNumberHolder is shown when guesses made are 1");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(2);
            display = GuessingGameLost.css("display");
            display2 = guessNumberHolder.css("display");
            disabled = guessLetter.attr("class").split(" ");
            t.equal(display, "none", "GameLost is hidden when guesses made are 2");
            t.equal(display2, "flex", "guessNumberHolder is shown when guesses made are 2");
            t.ok(!disabled.includes(disabled), "guessLetter(s) are not disabled");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(3);
            display = GuessingGameLost.css("display");
            display2 = guessNumberHolder.css("display");
            t.equal(display, "block", "GameLost is shown when guesses made are 3");
            t.equal(display2, "none", "guessNumberHolder is hidden when guesses made are 3");
            mock_Reset_GuessingGame();

        });

        test("setGuessesMade_CorrectValueSet", t => {
            GuessingGame.setGuessesMade(0);
            t.equal( GuessingGame.guessesMade,0, "GuessingGame.setGuessesMade has set GuessingGame.guessesMade to 0");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(1);
            t.equal( GuessingGame.guessesMade,1, "GuessingGame.setGuessesMade has set GuessingGame.guessesMade to 1");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(2);
            t.equal( GuessingGame.guessesMade,2, "GuessingGame.setGuessesMade has set GuessingGame.guessesMade to 2");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(3);
            t.equal( GuessingGame.guessesMade,3, "GuessingGame.setGuessesMade has set GuessingGame.guessesMade to 3");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(4);
            t.equal( GuessingGame.guessesMade,3, "GuessingGame.setGuessesMade has set GuessingGame.guessesMade to 3, since 4 was to large");
            mock_Reset_GuessingGame();

        });

        test("setGuessesMade_IncorrectValueSet", t => {

            try {
                GuessingGame.setGuessesMade(-1);
            }
            catch(e)
            {
                t.equal(e.message, "Value out of bounds", "GuessingGame.setGuessesMade(-1) has thrown an error successfully");
            }
            mock_Reset_GuessingGame();

        });

        test("setGuessesMade_guessCountDownShown", t => {

            GuessingGame.setGuessesMade(0);
            let display = $(`#GuessCounter3`).css("display");
            t.equal(display, "block", "GuessCounter3 for guess 1 is shown");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(1);
            display = $(`#GuessCounter2`).css("display");
            t.equal(display, "block", "GuessCounter2 for guess 2 is shown");
            mock_Reset_GuessingGame();

            GuessingGame.setGuessesMade(2);
            display = $(`#GuessCounter1`).css("display");
            t.equal(display, "block", "GuessCounter1 for guess 3 is shown");
            mock_Reset_GuessingGame();

        });

        test("f_NewNumber_Reset", t => {

            let guessNumberHolder = $("#guessNumberHolder");
            let GuessingGameWon = $("#GuessingGameWon");
            let GuessingGameLost = $("#GuessingGameLost");

            // Fucks up the page to see if f_NewWord can reset it properly
            const guess = $(".guessLetter");
            guess.addClass("correct");
            guess.addClass("wrong");
            guessNumberHolder.hide();
            GuessingGameWon.show();
            GuessingGameLost.show();
            GuessingGame.setGuessesMade(100);


            GuessingGame.f_newGame();

            // Asserts if the correct, wrong and disabled classes have been removed.
            let classes = $(`.guessNumber`).attr("class").split(" ");
            t.ok(!classes.includes("correct"), "guessLetter does not have class correct");
            t.ok(!classes.includes("wrong   "), "guessLetter does not have class wrong");

            // Checks that guessNumberHolder has been restored
            let display = guessNumberHolder.css("display");
            t.equal(display, "flex", "guesNumberholder is shown");

            // Checks that GuessingGameWon has been restored
            display = GuessingGameWon.css("display");
            t.equal(display, "none", "GuessingGameWon does not show");

            // Checks that GuessingGameWon has been restored
            display = GuessingGameLost.css("display");
            t.equal(display, "none", "GuessingGameLost does not show");

            // Checks the number of guesses made is reset to 0
            t.equal(GuessingGame.guessesMade, 0, "guessesMade is reset to 0");
            mock_Reset_GuessingGame();

        });

        test("f_NewWord_NumberCreation", t => {

            GuessingGame.anwser = null;
            GuessingGame.f_newGame();

            t.notEqual(GuessingGame.anwser, null, "GuessingGame.answe is set correct");

            mock_Reset_GuessingGame();
        });

        test("f_NewWord_ClickTest_WordCreation", t => {

            GuessingGame.anwser = null;
            $("#newNumber").trigger("click");
            t.notEqual(GuessingGame.anwser, null, "GuessingGame.answe is set correct");

            mock_Reset_GuessingGame();
        });

        test("f_checkGuess_TestWin", t => {

            GuessingGame.f_newGame();
            GuessingGame.anwser = 1;
            GuessingGame.f_checkGuess("1");


            // Checks that GuessingGameWon has been restored
            let display = $("#GuessingGameWon").css("display");
            t.equal(display, "block", "GuessingGameWon shown, game won");
            mock_Reset_GuessingGame();

        });

        test("f_checkGuess_TestLose", t => {

            GuessingGame.f_newGame();
            GuessingGame.anwser = 1;
            GuessingGame.f_checkGuess("2");
            GuessingGame.f_checkGuess("3");
            GuessingGame.f_checkGuess("4");

            // Checks that GuessingGameLost has been restored
            let display = $("#GuessingGameLost").css("display");
            t.equal(display, "block", "GuessingGameLost shown, game lost");
            mock_Reset_GuessingGame();

        });

        test("f_checkGuess_TestDuplicateGuess", t => {

            GuessingGame.f_newGame();
            GuessingGame.anwser = 7;
            GuessingGame.f_checkGuess("7");

            let HTML = $("html");
            let html = HTML.html();
            GuessingGame.f_checkGuess("7");

            t.deepEqual(html, HTML.html(), "Two correct guesses made");
            mock_Reset_GuessingGame();

            GuessingGame.f_newGame();
            GuessingGame.anwser = 6;
            GuessingGame.f_checkGuess("7");

            html = HTML.html();
            GuessingGame.f_checkGuess("7");

            t.deepEqual(html, HTML.html(), "Two incorrect guesses made");
            mock_Reset_GuessingGame();

        });

        test("f_checkGuess_TestHighGuess", t => {

            GuessingGame.f_newGame();
            GuessingGame.anwser = 1;
            GuessingGame.f_checkGuess("10");

            let html = $(`#guessingGameGuess1`).html();

            t.deepEqual(html, `10 is larger`, "High guess gives correct result");

            GuessingGame.f_checkGuess("9");

            html = $(`#guessingGameGuess2`).html();

            t.deepEqual(html, `9 is larger`, "High guess gives correct result");
            mock_Reset_GuessingGame();

        });

        test("f_checkGuess_TestLowGuess", t => {

            GuessingGame.f_newGame();
            GuessingGame.anwser = 10;

            GuessingGame.f_checkGuess("1");
            let html = $(`#guessingGameGuess1`).html();
            t.deepEqual(html, `1 is smaller`, "Low guess gives correct result");

            GuessingGame.f_checkGuess("2");
            html = $(`#guessingGameGuess2`).html();
            t.deepEqual(html, `2 is smaller`, "Low guess gives correct result");

            mock_Reset_GuessingGame();

        });

        test("f_checkGuess_TestInvalidGuess", t => {

            GuessingGame.f_newGame();

            try{
                GuessingGame.f_checkGuess("Test");
            }
            catch (e) {
                let html = $(`#guessingGameGuess1`).html();
                t.deepEqual(html, "You idiot, that's not a number.", "Invalid guess gives correct result");
                t.equal(e, "f_checkGuess Invalid input");
            }


            try{
                GuessingGame.f_checkGuess("Test");
            }
            catch (e) {
                let html = $(`#guessingGameGuess2`).html();
                t.deepEqual(html, "You idiot, that's not a number.", "Invalid guess gives correct result");
                t.equal(e, "f_checkGuess Invalid input");
            }

            mock_Reset_GuessingGame();

        });

    });

    function mock_Reset_GuessingGame() {
        $("#guessCountDown").children().hide();
        $("#GuessCounter3").show();
        $(".guessNumber").removeClass("correct").removeClass("wrong");
        $("#guessNumberHolder").show();
        GuessingGame.guessesMade = 0;
        GuessingGame.anwser = null;
    }
});
