"use strict";

let GuessColor = {};
$(document).ready(function () {

    GuessColor.NameHolder = $("#NameHolder");
    GuessColor.RGBHolder = $("#RGBHolder");
    GuessColor.HexHolder = $("#HexHolder");
    GuessColor.ColourHolder = $("#ColourHolder");
    GuessColor.StatusHolder = $("#StatusHolder");
    GuessColor.C1 = Colour.getRandomColour();
    GuessColor.C2 = Colour.getRandomColour();
    GuessColor.C3 = Colour.getRandomColour();
    GuessColor.Correct = GuessColor.C1;
    GuessColor.gameMode = "playName2RGB";


    GuessColor.reset = function () {
        GuessColor.NameHolder.html("");
        GuessColor.RGBHolder.html("");
        GuessColor.HexHolder.html("");
        GuessColor.ColourHolder.html("");
        GuessColor.C1 = Colour.getRandomColour();
        GuessColor.C2 = Colour.getRandomColour();
        GuessColor.C3 = Colour.getRandomColour();

        switch (Library.getRandomInt(0, 2)) {
            case 0:
                GuessColor.Correct = GuessColor.C1;
                break;
            case 1:
                GuessColor.Correct = GuessColor.C2;
                break;
            case 2:
                GuessColor.Correct = GuessColor.C3;
                break;
        }
    };

    GuessColor.attachListeners = function () {
        $("#guessC1").on("click", function () {
            GuessColor.guess(1);
        });
        $("#guessC2").on("click", function () {
            GuessColor.guess(2);
        });
        $("#guessC3").on("click", function () {
            GuessColor.guess(3);
        });
    };

    GuessColor.guess = function (i) {
        if (eval(`GuessColor.C${i}.getHex()`) === GuessColor.Correct.getHex()) {
            GuessColor.StatusHolder.html("You have won.");
        } else {
            GuessColor.StatusHolder.html("You have lost, loser.");
        }
        GuessColor[GuessColor.gameMode]();
    };

    GuessColor.attachMenuListener = function () {
        $(".gamePicker").on("click", function (e) {
            let cls = $(e.target).attr("class");
            if(cls !== "gamePicker"){
                let name = $(e.target).html();
                GuessColor[`${name}`]();
                GuessColor.gameMode = name;
            }
        });
    };
    GuessColor.attachMenuListener();



    GuessColor.playName2RGB = function () {
        GuessColor.reset();
        GuessColor.NameHolder.html(`<p>${GuessColor.Correct.getName()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC1">${GuessColor.C1.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC2">${GuessColor.C2.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC3">${GuessColor.C3.getRGB().toString()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playName2Hex = function () {
        GuessColor.reset();
        GuessColor.NameHolder.html(`<p>${GuessColor.Correct.getName()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC1">${GuessColor.C1.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC2">${GuessColor.C2.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC3">${GuessColor.C3.getHex()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playName2Colour = function () {
        GuessColor.reset();
        GuessColor.NameHolder.html(`<p>${GuessColor.Correct.getName()}</p>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC1" style="background: rgb(${GuessColor.C1.getR()},${GuessColor.C1.getG()},${GuessColor.C1.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC2" style="background: rgb(${GuessColor.C2.getR()},${GuessColor.C2.getG()},${GuessColor.C2.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC3" style="background: rgb(${GuessColor.C3.getR()},${GuessColor.C3.getG()},${GuessColor.C3.getB()})"></div>`);
        GuessColor.attachListeners();
    };


    GuessColor.playRGB2Name = function () {
        GuessColor.reset();
        GuessColor.RGBHolder.html(`<p>${GuessColor.Correct.getRGB().toString()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC1">${GuessColor.C1.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC2">${GuessColor.C2.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC3">${GuessColor.C3.getName()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playRGB2Hex = function () {
        GuessColor.reset();
        GuessColor.RGBHolder.html(`<p>${GuessColor.Correct.getRGB().toString()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC1">${GuessColor.C1.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC2">${GuessColor.C2.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC3">${GuessColor.C3.getHex()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playRGB2Colour = function () {
        GuessColor.reset();
        GuessColor.RGBHolder.html(`<p>${GuessColor.Correct.getRGB().toString()}</p>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC1" style="background: rgb(${GuessColor.C1.getR()},${GuessColor.C1.getG()},${GuessColor.C1.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC2" style="background: rgb(${GuessColor.C2.getR()},${GuessColor.C2.getG()},${GuessColor.C2.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC3" style="background: rgb(${GuessColor.C3.getR()},${GuessColor.C3.getG()},${GuessColor.C3.getB()})"></div>`);
        GuessColor.attachListeners();
    };


    GuessColor.playHex2Name = function () {
        GuessColor.reset();
        GuessColor.HexHolder.html(`<p>${GuessColor.Correct.getHex()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC1">${GuessColor.C1.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC2">${GuessColor.C2.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC3">${GuessColor.C3.getName()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playHex2RGB = function () {
        GuessColor.reset();
        GuessColor.HexHolder.html(`<p>${GuessColor.Correct.getHex()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC1">${GuessColor.C1.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC2">${GuessColor.C2.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC3">${GuessColor.C3.getRGB().toString()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playHex2Colour = function () {
        GuessColor.reset();
        GuessColor.HexHolder.html(`<p>${GuessColor.Correct.getHex()}</p>`);
        GuessColor.ColourHolder.append(`<div  class="guessColor" id="guessC1" style="background: rgb(${GuessColor.C1.getR()},${GuessColor.C1.getG()},${GuessColor.C1.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div  class="guessColor" id="guessC2" style="background: rgb(${GuessColor.C2.getR()},${GuessColor.C2.getG()},${GuessColor.C2.getB()})"></div>`);
        GuessColor.ColourHolder.append(`<div class="guessColor" id="guessC3" style="background: rgb(${GuessColor.C3.getR()},${GuessColor.C3.getG()},${GuessColor.C3.getB()})"></div>`);
        GuessColor.attachListeners();
    };


    GuessColor.playColour2Name = function () {
        GuessColor.reset();
        GuessColor.ColourHolder.html(`<div class="guessColor" style="background: rgb(${GuessColor.Correct.getR()},${GuessColor.Correct.getG()},${GuessColor.Correct.getB()})"></div>`);
        GuessColor.NameHolder.append(`<p id="guessC1">${GuessColor.C1.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC2">${GuessColor.C2.getName()}</p>`);
        GuessColor.NameHolder.append(`<p id="guessC3">${GuessColor.C3.getName()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playColour2RGB = function () {
        GuessColor.reset();
        GuessColor.ColourHolder.html(`<div class="guessColor" style="background: rgb(${GuessColor.Correct.getR()},${GuessColor.Correct.getG()},${GuessColor.Correct.getB()})"></div>`);
        GuessColor.RGBHolder.append(`<p id="guessC1">${GuessColor.C1.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC2">${GuessColor.C2.getRGB().toString()}</p>`);
        GuessColor.RGBHolder.append(`<p id="guessC3">${GuessColor.C3.getRGB().toString()}</p>`);
        GuessColor.attachListeners();
    };

    GuessColor.playColour2Hex = function () {
        GuessColor.reset();
        GuessColor.ColourHolder.html(`<div class="guessColor" style="background: rgb(${GuessColor.Correct.getR()},${GuessColor.Correct.getG()},${GuessColor.Correct.getB()})"></div>`);
        GuessColor.HexHolder.append(`<p id="guessC1">${GuessColor.C1.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC2">${GuessColor.C2.getHex()}</p>`);
        GuessColor.HexHolder.append(`<p id="guessC3">${GuessColor.C3.getHex()}</p>`);
        GuessColor.attachListeners();
    };



});

