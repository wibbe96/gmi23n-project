$(document).ready(function () {

    QUnit.module("Color", function () {

        const {test} = QUnit;

        test("Color_constructor", t => {
            let col = Colour.getRandomColour();
            t.equal(typeof col, "object", "Colour.getRandomColour creates an object");
            t.ok(col.Name !== "", "Colour.getRandomColour creates an object with a name");
            t.ok(col.Hex !== "", "Colour.getRandomColour creates an object with a hex value");
            t.ok(col.RGB !== "", "Colour.getRandomColour creates an object with an rgb value");
        });

        test("Color_getName", t => {
            let col = Colour.getRandomColour();
            t.notEqual(col.getName(), "", "Colour.getName returns a name: " + col.getName());
        });

        test("Color_getHex", t => {
            let col = Colour.getRandomColour();
            t.equal(col.getHex().toString().split("").length, 6, "Colour.getHex returns a hex value of the correct length.");
        });

        test("Color_getRGB", t => {
            let col = Colour.getRandomColour();
            t.equal(col.getRGB().length, 3, "Colour.gerRGB returns an rgb value of the correct length.");
        });

        test("Color_getR", t => {
            let col = Colour.getRandomColour();
            t.equal(col.getR(), col.getRGB()[0], "Colour.getR returns an int value.");
            t.ok(col.getR() > -1, "Colour.getR returns an int value of zero or above");
            t.ok(col.getR() < 256, "Colour.getR returns an int value of 255 or bellow.");
        });

        test("Color_getG", t => {
            let col = Colour.getRandomColour();
            t.equal(col.getG(), col.getRGB()[1], "Colour.getG returns an int value.");
            t.ok(col.getG() > -1, "Colour.getG returns an int value of zero or above");
            t.ok(col.getG() < 256, "Colour.getG returns an int value of 255 or bellow.");
        });

        test("Color_getB", t => {
            let col = Colour.getRandomColour();
            t.equal(col.getB(), col.getRGB()[2], "Colour.getB returns an int value.");
            t.ok(col.getB() > -1, "Colour.getB returns an int value of zero or above");
            t.ok(col.getB() < 256, "Colour.getB returns an int value of 255 or bellow.");
        });



    });


});